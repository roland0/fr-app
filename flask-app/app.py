from flask import Flask
from redis import Redis
import os

app = Flask(__name__)
#redis_master = Redis(host=os.environ.get('REDIS_MASTER_SERVICE_HOST'), port=6379)
#redis_slave = Redis(host=os.environ.get('REDIS_SLAVE_SERVICE_HOST'), port=6379)

redis_master = Redis(host='redis-master', port=6379)
redis_slave = Redis(host='redis-slave', port=6379)

hostname = os.uname()[1]



@app.route('/')
def print_hostname():
    redis_master.incr('hits')
    count = redis_slave.get('hits')
    return "<p> Hostname: {hostname} </p> <br/> <p> Hits: {count} </p> ".format(hostname=hostname, count=count)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
