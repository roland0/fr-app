Table of Contents
=================

   * [Intro](#basic-intro)
   * [Prerequisites](#Prerequisites)


# Intro
This repo contains a simple web app that uses a database to count the number of 'hits' it has recieved. It also has tools to deploy it. It has been tested on Linux and OSX.

# Prerequisites
  * git        - version control system instalation instructions [here](https://git-scm.com/book/id/v2/Getting-Started-Installing-Git)
  * Virtualbox - cross platform tool that can vun virtual machines installation instructions [here](https://www.virtualbox.org/wiki/Downloads)
  * Minikube   - cross platform tool that makes setting up a single node Kubernetes cluster simple. Installation instructions [here](https://kubernetes.io/docs/tasks/tools/install-minikube/)
  * kubectrl   - a tool to control a kubnernetes cluster installation instructions [here](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

# Instructions


Firstly clone this repository via `git clone git@bitbucket.org:roland0/fr-app.git` when in the directory, typically `cd fr-app` you should see several numbered `.yaml` files. These are used to define the various services to make the app run.

### Deploying the database - Master

To start the database (master) simply type the following:

`kubectrl create -f 01-redis-master-deployment.yaml`

If you now run the command `kubectrl get deployments` you should see something similar to the below:


After a few minutes (depending on your internet connection speed - it could take up to 10 minutes) you should see that the deployment becomes 'available'

### Deploying Database - Slave

This is almost identical to deploying the Master simply type the following

 kubectrl create -f kubectrl create -f 02-redis-slave-deployment.yaml

 Again after a few minutes you will be able to see the deployment as being available:

## Deploying the app

 All you need to do to deploy the app is to run the command:

 `kubectl create -f 03-app-deployment.yaml`

 And that's it!


# Viewing the app.

The application is effectively running in a VM on your local machine, to get the address, simply type:

`minikube service fr-hostname-lb --url`

And it will return somthing similar to the following


`http://192.168.99.100:32697`

Simply copy and paste this into a web browser (you might be able to click on it).

You should see a (not very pretty) website that will display the container hostname, and it will show the number of hits. If you open another browser (ie if you where using Safari try chrome) you should see the same. You should notice that the hostname will change betweeen the two hosts.

### Scaling the app.


To increase the number of containers running the app, type the following:

`kubectl scale deployment fr-hostname --replicas=7`

After a few seconds you'll notice that the application when viewing the application you'll see a greater variety of hostnames (press F5 to refresh the browser)

you can also see the containers by typing:

`kubectl get pods`
